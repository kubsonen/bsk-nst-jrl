import React from 'react';
import './App.css';
import Cryptography from "../cryptography/cryptography";

function App() {
    return (
        <Cryptography/>
    );
}

export default App;
