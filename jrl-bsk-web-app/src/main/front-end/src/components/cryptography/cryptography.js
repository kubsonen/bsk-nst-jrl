import * as React from "react";
import './cryptography.css';
import {Polynomial} from "../polynomial/polynomial";

const codeType = {
    RAIL_FENCE: 'fail-fence',
    MATRIX_TRANSFORMATION: 'matrix-transformation',
    CESAR_CODE: 'cesar-code',
    VIGENERE_CODE: 'vigenere-code',
    STREAM_CIPHER: 'stream-cipher'
};

class Cryptography extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            codeType: codeType.RAIL_FENCE,
            encryption: true,
            example: '',
            shift: 0,
            key: '',
            polynomialValues: [],
            numberOfBits: 0,
            input: '',
            result: '',
            error: ''
        };
    }

    switchEncryption() {
        this.setState(state => {
            state.encryption = !state.encryption;
            return state;
        })
    }

    changeCodeType(codeType) {
        this.setState(state => {
            state.error = undefined;
            state.codeType = codeType;
            return state;
        })
    };

    handleChange(e, prop) {
        let val = e.target.value;
        this.setState(state => {
            state.error = undefined;
            state[prop] = val;
            return state;
        });
    }

    handlePolynomialChange(v) {
        this.state.polynomialValues = v;
    }

    callApi() {

        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                codeType: this.state.codeType,
                encryption: this.state.encryption,
                example: this.state.example,
                shift: this.state.shift,
                key: this.state.key,
                input: this.state.input,
                polynomialValues: this.state.polynomialValues,
                numberOfBits: this.state.numberOfBits
            })
        };

        // fetch("http://localhost:8122/api/crypt", requestOptions)
        fetch("/api/crypt", requestOptions)
            .then(value => value.json())
            .then(obj => {
                if (obj.error === false) {
                    this.setState(state => {
                        state.result = obj['txt'];
                        state.error = '';
                        return state;
                    })
                } else {
                    this.setState(state => {
                        state.error = obj['txt'];
                        state.result = '';
                        return state;
                    })
                }
            });

    }

    render() {
        return (
            <div className="container page-top-margin">
                <h1 className="title">Cryptography algorithms</h1>
                <div className="columns">
                    <div className="column">
                        <div className="buttons has-addons">
                            <button
                                className={"button " + (this.state.codeType === codeType.RAIL_FENCE ? 'is-primary' : '')}
                                onClick={() => this.changeCodeType(codeType.RAIL_FENCE)}>Rail fence
                            </button>
                            <button
                                className={"button " + (this.state.codeType === codeType.MATRIX_TRANSFORMATION ? 'is-primary' : '')}
                                onClick={() => this.changeCodeType(codeType.MATRIX_TRANSFORMATION)}>Matrix
                                transformation
                            </button>
                            <button
                                className={"button " + (this.state.codeType === codeType.CESAR_CODE ? 'is-primary' : '')}
                                onClick={() => this.changeCodeType(codeType.CESAR_CODE)}>Caesar cipher
                            </button>
                            <button
                                className={"button " + (this.state.codeType === codeType.VIGENERE_CODE ? 'is-primary' : '')}
                                onClick={() => this.changeCodeType(codeType.VIGENERE_CODE)}>Vigenere cipher
                            </button>
                            <button
                                className={"button " + (this.state.codeType === codeType.STREAM_CIPHER ? 'is-primary' : '')}
                                onClick={() => this.changeCodeType(codeType.STREAM_CIPHER)}>Stream cipher
                            </button>
                        </div>
                    </div>
                </div>

                <div className="columns">
                    <div className="column">
                        {this.state.codeType === codeType.STREAM_CIPHER ?
                            (<div className="field">
                                <label className="label">Polynomial elements</label>
                                <div className="control">
                                    <input className="input" type="number" placeholder="" value={this.state.polynomialElements}
                                           onChange={e => this.handleChange(e, 'polynomialElements')}/>
                                </div>
                                <div className="field">
                                    <Polynomial polynomialElements={this.state.polynomialElements}
                                                polynomialChangeValue={v => this.handlePolynomialChange(v)}/>
                                </div>
                            </div>) : null}
                        {this.state.codeType === codeType.MATRIX_TRANSFORMATION ?
                            (<div className="field">
                                <label className="label">Example</label>
                                <div className="control">
                                    <div className="select">
                                        <select value={this.state.example}
                                                onChange={e => this.handleChange(e, 'example')}>
                                            <option value={''}/>
                                            <option value={'A'}>A</option>
                                            <option value={'B'}>B</option>
                                            <option value={'C'}>C</option>
                                        </select>
                                    </div>
                                </div>
                            </div>) : null}
                        {this.state.codeType === codeType.RAIL_FENCE || this.state.codeType === codeType.CESAR_CODE ?
                            (<div className="field">
                                <label className="label">Shift</label>
                                <div className="control">
                                    <input className="input" type="number" placeholder="" value={this.state.shift}
                                           onChange={e => this.handleChange(e, 'shift')}/>
                                </div>
                            </div>) : null}
                        {this.state.codeType === codeType.MATRIX_TRANSFORMATION ||
                        this.state.codeType === codeType.STREAM_CIPHER ||
                        this.state.codeType === codeType.VIGENERE_CODE ?
                            (<div className="field">
                                <label className="label">Key</label>
                                <div className="control">
                                    <input className="input" type="text" placeholder="" value={this.state.key}
                                           onChange={e => this.handleChange(e, 'key')}/>
                                </div>
                            </div>) : null}
                        <div className="field">
                            <label className="label">Input</label>
                            <div className="control">
                                <textarea className="textarea" value={this.state.input}
                                          onChange={e => this.handleChange(e, 'input')}/>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">
                                {this.state.encryption === true ?
                                    "Encrypted" : "Decrypted"}
                            </label>
                            <div className="control">
                                <textarea className="textarea" readOnly={true} value={this.state.result}
                                          onChange={e => this.handleChange(e, 'result')}/>
                            </div>
                        </div>
                    </div>
                </div>

                {(this.state.error !== '' && this.state.error !== undefined && this.state.error !== null) ?
                    (<div className="notification is-danger">
                        {this.state.error}
                    </div>)
                    : null}

                <div className="columns">
                    <div className="column">
                        <div className="buttons has-addons">
                            <button className={'button is-info'} onClick={e => this.switchEncryption()}>Switch</button>
                            <button className={'button is-primary'} onClick={e => this.callApi(this.state)}>
                                {this.state.encryption === true ?
                                    "Encrypt" : "Decrypt"}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default Cryptography;
