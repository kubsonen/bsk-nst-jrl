import * as React from "react";
import './polynomial.css';

export class Polynomial extends React.Component {
    polynomialElements;
    polynomialChangeValue;

    constructor(props) {
        super(props);
        this.state = {
            values: [-1, -1, -1, -1, -1]
        };
        this.changeValues(this.state, props);
    }

    changeValue(index, value) {
        this.setState(state => {
            state.values[index] = value;
            this.changeValues(state, this.props);
            return state;
        });
    }

    changeValues(state, props) {
        let arr = [];
        if (props.polynomialElements && props.polynomialElements > 0 && props.polynomialElements <= 5) {
            for (let i = 0; i < props.polynomialElements; i++) {
                arr.push(state.values[i]);
            }
        }
        if (this.props.polynomialChangeValue) {
            this.props.polynomialChangeValue(arr);
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState(state => {
            for (let i = (nextProps.polynomialElements); i < state.values.length; i++) {
                state.values[i] = -1;
            }
            this.changeValues(state, nextProps);
            return state;
        });
    }

    render() {
        const elements = [];
        if (this.props.polynomialElements && this.props.polynomialElements > 0 && this.props.polynomialElements <= 5) {
            elements.push(
                <div key={'oneNumber'} className={'polynomial-element'}>
                    <p>1</p>
                </div>);
            for (let i = 1; i <= this.props.polynomialElements; i++) {
                elements.push(
                    <div key={'polynomialPlus' + i} className={'polynomial-element'}>
                        <p>+</p>
                    </div>
                );
                elements.push(
                    <div key={'polynomialElement' + i} className={'polynomial-input-element'}>
                        <p>X</p>
                        <input type={'number'} value={this.state.values[i - 1]}
                               onChange={e => this.changeValue(i - 1, e.target.value)}/>
                    </div>
                );
            }
        } else {
            elements.push(<p key={'polynomialError'}>Required index size between 0 and 5!</p>);
        }

        return (<div style={{marginTop: "15px"}}>{elements}</div>);
    }

}
