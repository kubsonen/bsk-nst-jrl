package pl.jrl.bsk.controller;

import org.springframework.web.bind.annotation.*;
import pl.jrl.bsk.crypt.*;
import pl.jrl.bsk.model.ApiRequest;
import pl.jrl.bsk.model.ApiResponse;
import pl.jrl.bsk.model.CodeType;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api")
public class ApiController {

    @PostMapping(value = "crypt")
    public ApiResponse test(@RequestBody ApiRequest apiRequest) {

        try {
            CodeType ct = apiRequest.getCodeType();
            if (ct == null)
                throw new RuntimeException("Not found code type");

            String message = apiRequest.getInput();
            if (message == null)
                throw new RuntimeException("Not found input");

            String example = apiRequest.getExample();
            String key = apiRequest.getKey();
            Long shift = apiRequest.getShift();
            Boolean encrypt = apiRequest.getEncryption();
            List<Integer> polynomialElements = apiRequest.getPolynomialValues();

            Cryptography c = null;
            switch (ct) {
                case RAIL_FENCE:
                    c = new RailFence(shift, message);
                    break;
                case MATRIX_TRANSFORMATION:
                    c = new MatrixTransformation(example, key, message);
                    break;
                case CESAR_CODE:
                    c = new CaesarCipher(shift, message);
                    break;
                case VIGENERE_CODE:
                    c = new VigenereCipher(key, message);
                    break;
                case STREAM_CIPHER:
                    c = new StreamCipher(polynomialElements, key, message);
                    break;
            }

            if (encrypt == null || encrypt)
                return ApiResponse.success(c.encrypt());
            else
                return ApiResponse.success(c.decrypt());

        } catch (Throwable t) {
            t.printStackTrace();
            return ApiResponse.error(t.getClass().getName() + " - " + t.getMessage());
        }

    }

}

