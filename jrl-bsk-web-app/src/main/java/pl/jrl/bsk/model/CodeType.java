package pl.jrl.bsk.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

@JsonDeserialize(using = CodeType.CodeTypeEnumDeserializer.class)
public enum CodeType {

    RAIL_FENCE("fail-fence"),
    MATRIX_TRANSFORMATION("matrix-transformation"),
    CESAR_CODE("cesar-code"),
    VIGENERE_CODE("vigenere-code"),
    STREAM_CIPHER("stream-cipher");

    private String type;

    CodeType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static CodeType getByType(String type) {
        for (CodeType ct : CodeType.values())
            if (ct.type.equals(type))
                return ct;
        return null;
    }

    public static class CodeTypeEnumDeserializer extends StdDeserializer<CodeType> {

        CodeTypeEnumDeserializer() {
            super(CodeType.class);
        }

        @Override
        public CodeType deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            JsonNode node = jsonParser.getCodec().readTree(jsonParser);
            return CodeType.getByType(node.asText());
        }
    }

}
