package pl.jrl.bsk.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class ApiRequest {
    private CodeType codeType;
    private Boolean encryption;
    private String example;
    private Long shift;
    private String key;
    private String input;
    private List<Integer> polynomialValues;
}
