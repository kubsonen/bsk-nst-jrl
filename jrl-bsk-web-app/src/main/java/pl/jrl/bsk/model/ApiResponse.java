package pl.jrl.bsk.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiResponse {
    private boolean error;
    private String txt;

    public ApiResponse(boolean error, String txt) {
        this.error = error;
        this.txt = txt;
    }

    public static ApiResponse error(String txt) {
        return new ApiResponse(true, txt);
    }

    public static ApiResponse success(String txt) {
        return new ApiResponse(false, txt);
    }
}
