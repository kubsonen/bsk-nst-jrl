package pl.jrl.bsk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JrlBskApplication {

    public static void main(String[] args) {
        SpringApplication.run(JrlBskApplication.class, args);
    }

}
