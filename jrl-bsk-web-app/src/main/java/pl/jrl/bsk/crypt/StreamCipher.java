package pl.jrl.bsk.crypt;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class StreamCipher implements Cryptography {

    private final char[] key;
    private final String message;
    private final Integer maxElement;
    private final Integer[] entryBits;
    private final List<Integer> elementValues;
    private static final Integer MAX_NUMBER_APPEND_LEN = 2;

    public StreamCipher(List<Integer> elementValues, String key, String message) {

        if (message == null || message.trim().isEmpty())
            throw new RuntimeException("Not found message");

        this.message = message.trim();
        this.elementValues = elementValues;

        if (key == null || key.trim().isEmpty())
            throw new RuntimeException("Please insert key");

        this.key = key.trim().toCharArray();

        if (elementValues == null || elementValues.isEmpty())
            throw new RuntimeException("Please insert polynomial elements");

        if (elementValues.stream().anyMatch(i -> i <= 0))
            throw new RuntimeException("Found polynomial elements less than one");

        if (new HashSet<>(elementValues).size() != elementValues.size())
            throw new RuntimeException("Found duplicate polynomial elements");

        this.maxElement = elementValues
                .stream()
                .max(Integer::compareTo)
                .orElseThrow(() -> new RuntimeException("Not found maximum value"));

        if (this.maxElement != key.length())
            throw new RuntimeException("Please insert key with length equal to the highest value of polynomial element");

        for (char c : this.key)
            if (!(c == '1' || c == '0'))
                throw new RuntimeException("Key must contain elements with value 0 and 1");

        this.entryBits = new Integer[maxElement];
        for (int i = 0; i < this.entryBits.length; i++) this.entryBits[i] = Integer.valueOf(this.key[i] + "");
    }

    @Override
    public String encrypt() {

        //Message bits
        List<Character> characters = convertMessageToBitArray(message, 0, false);
        List<Character> charactersWithoutSpaces = characters.stream().filter(c -> c != ' ').collect(Collectors.toList());

        //Create a bit stream
        Integer[][] bitMatrix = new Integer[charactersWithoutSpaces.size()][];
        Integer[] bitStream = new Integer[charactersWithoutSpaces.size()];
        buildXorMatrix(bitMatrix, bitStream);

        char[] characterArray = new char[characters.size()];
        for (int i = 0; i < characters.size(); i++) characterArray[i] = characters.get(i);
        String encryptXorBits = getXorBits(characterArray, bitStream);
        return convertBitArrayToString(encryptXorBits, 255, true);
    }

    private Integer xorResult(Integer... xorIngredients) {
        Boolean[] booleans = new Boolean[xorIngredients.length];
        for (int i = 0; i < booleans.length; i++) {
            booleans[i] = (xorIngredients[i] == 1);
        }
        Boolean result = booleans[0] ^ booleans[1];
        for (int i = 2; i < booleans.length; i++) {
            result = result ^ booleans[i];
        }
        return result ? 1 : 0;
    }

    public static List<Character> convertMessageToBitArray(String message, int offset, boolean postLen) {
        List<Character> characters = new ArrayList<>();
        char[] messageChars = message.toCharArray();
        for (int i = 0; i < messageChars.length; i++) {

            Integer bitLen = null;
            if (postLen) {
                StringBuilder sbBitLenNumber = new StringBuilder();
                for (int j = 0; j < MAX_NUMBER_APPEND_LEN; j++) {
                    sbBitLenNumber.append(messageChars[i]);
                    i++;
                }
                bitLen = Integer.valueOf(sbBitLenNumber.toString());
            }

            int charNumber = ((int) messageChars[i]) + offset;
            String numberBinaryRep = Integer.toBinaryString(charNumber);

            if (bitLen == null)
                bitLen = numberBinaryRep.length();

            char[] charNumberBits = numberBinaryRep.toCharArray();
            char[] fullCharNumberBit = fillBits(charNumberBits, bitLen);

            for (char bit : fullCharNumberBit) characters.add(bit);
            if (i != (messageChars.length - 1)) characters.add(' ');
        }
        return characters;
    }

    public static char[] fillBits(char[] charNumberBit, Integer fillTo) {
        char[] fullCharNumberBit = new char[fillTo];
        for (int i = 0; i < fullCharNumberBit.length; i++)
            fullCharNumberBit[i] = '0';
        for (int j = (charNumberBit.length - 1); j >= 0; j--)
            fullCharNumberBit[fullCharNumberBit.length - 1 - (charNumberBit.length - j - 1)] = charNumberBit[j];
        return fullCharNumberBit;
    }

    public static String fillZero(String text, Integer fullLen) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < fullLen; i++)
            sb.append("0");
        sb.append(text);
        String s = sb.toString();
        return s.substring(s.length() - fullLen);

    }

    public static String convertBitArrayToString(String bitArray, int offset, boolean appendLen) {
        String[] parts = bitArray.split(" ");
        StringBuilder sb = new StringBuilder();
        for (String s : parts) {
            if (appendLen)
                sb.append(fillZero(String.valueOf(s.length()), MAX_NUMBER_APPEND_LEN));
            if (!s.isEmpty()) {
                int charNumber = Integer.parseInt(s, 2) + offset;
                sb.append((char) charNumber);
            }
        }
        return sb.toString();
    }

    @Override
    public String decrypt() {

        //Message bits
        Character[] cArray = convertMessageToBitArray(message, -255, true).toArray(new Character[0]);

        for (char c : cArray)
            if (!(c == '1' || c == '0' || c == ' '))
                throw new RuntimeException("Message to decrypt should contain 0, 1 or space");

        List<Character> cArrayWithoutSpaces = new ArrayList<>();
        for (char c : cArray)
            if (c != ' ')
                cArrayWithoutSpaces.add(c);

        int cNoSpaceLen = cArrayWithoutSpaces.size();

        //Create a bit stream
        Integer[][] bitMatrix = new Integer[cNoSpaceLen][];
        Integer[] bitStream = new Integer[cNoSpaceLen];
        buildXorMatrix(bitMatrix, bitStream);

        char[] copyArray = new char[cArray.length];
        for (int i = 0; i < cArray.length; i++) copyArray[i] = cArray[i];
        return convertBitArrayToString(getXorBits(copyArray, bitStream), 0, false);

    }

    private void buildXorMatrix(Integer[][] bitMatrix, Integer[] bitStream) {
        for (int i = 0; i < bitMatrix.length; i++) {
            if (i == 0)
                bitMatrix[i] = this.entryBits;
            else {
                Integer[] xorIngredients = new Integer[elementValues.size()];
                for (int k = 0; k < xorIngredients.length; k++)
                    xorIngredients[k] = bitMatrix[i - 1][elementValues.get(k) - 1];
                Integer[] rowBits = new Integer[maxElement];
                rowBits[0] = xorResult(xorIngredients);
                for (int k = 1; k < rowBits.length; k++)
                    rowBits[k] = bitMatrix[i - 1][k - 1];
                bitMatrix[i] = rowBits;
            }
            bitStream[i] = bitMatrix[i][bitMatrix[i].length - 1];
        }
    }

    private String getXorBits(char[] cArray, Integer[] bitStream) {
        int j = 0;
        StringBuilder sbEncryptValue = new StringBuilder();
        for (Character c : cArray) {
            if (c != ' ') {
                int charDigit = Integer.valueOf(c + "");
                sbEncryptValue.append(xorResult(bitStream[j], charDigit));
                j++;
            } else {
                sbEncryptValue.append(" ");
            }
        }
        return sbEncryptValue.toString();
    }

}
