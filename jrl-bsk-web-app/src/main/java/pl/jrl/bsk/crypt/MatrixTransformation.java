package pl.jrl.bsk.crypt;

import java.util.*;

public class MatrixTransformation implements Cryptography {

    private final String example;
    private final String key;
    private final String message;

    private final char[] messageChars;
    private final int[] keyCharOrdinal;
    private final int[] keyCharOrdinalSort;
    private int[] orderDigits;

    public MatrixTransformation(String example, String key, String message) {
        this.example = example;
        this.key = key;
        this.message = message;

        if (this.example == null || this.example.trim().isEmpty())
            throw new RuntimeException("Please select example");

        messageChars = this.message.replace(" ", "").toCharArray();
        char[] keyChars = this.key.toCharArray();
        char[] sortedKeyChars = keyChars.clone();
        Arrays.sort(sortedKeyChars);
        keyCharOrdinal = new int[keyChars.length];
        for (int k = 0; k < sortedKeyChars.length; k++)
            for (int j = 0; j < keyChars.length; j++)
                if (keyChars[j] == sortedKeyChars[k]) {
                    keyCharOrdinal[j] = (k + 1);
                    keyChars[j] = '\0';
                    break;
                }
        keyCharOrdinalSort = keyCharOrdinal.clone();
        Arrays.sort(keyCharOrdinalSort);

        if ("A".equals(this.example))
            try {
                String[] orderDigitsTxt = key.split("-");
                orderDigits = new int[orderDigitsTxt.length];
                for (int i = 0; i < orderDigitsTxt.length; i++)
                    orderDigits[i] = Integer.valueOf(orderDigitsTxt[i]);
            } catch (Throwable t) {
                throw new RuntimeException("Required key pattern: 3-1-4-2");
            }

    }

    public String encrypt() {
        switch (example) {
            case "A":
                return exampleEncryptA();
            case "B":
                return exampleEncryptB();
            case "C":
                return exampleEncryptC();
        }

        throw new RuntimeException("Not found example '" +  example + "'");
    }

    public String decrypt() {
        switch (example) {
            case "A":
                return exampleDecryptA();
            case "B":
                return exampleDecryptB();
            case "C":
                return exampleDecryptC();
        }

        throw new RuntimeException("Not found example '" + example + "'");
    }

    private String exampleEncryptA() {

        int[] sortedOrderDigits = Arrays.stream(orderDigits).sorted().toArray();
        List<Map.Entry<Integer, Character>> intCharEntries = new ArrayList<>();
        char[] messageChars = message.toCharArray();

        for (int i = 0; i < messageChars.length; i++) {
            int digit = sortedOrderDigits[i % sortedOrderDigits.length];
            Map.Entry<Integer, Character> entry = new AbstractMap.SimpleEntry<>(digit, messageChars[i]);
            intCharEntries.add(entry);
        }

        StringBuilder sb = new StringBuilder();
        while (!intCharEntries.isEmpty()) {
            for (int orderDigit : orderDigits) {
                Map.Entry toRemove = null;
                for (Map.Entry e : intCharEntries)
                    if (e.getKey().equals(orderDigit)) {
                        toRemove = e;
                        break;
                    }
                if (toRemove != null) {
                    sb.append(toRemove.getValue());
                    intCharEntries.remove(toRemove);
                }
            }
        }

        return sb.toString();
    }

    private String getEntriesTxtByOrder(List<Map.Entry<Integer, Character>> intCharEntries) {
        StringBuilder sb = new StringBuilder();
        for (int sortDigit : keyCharOrdinalSort)
            for (Map.Entry e : intCharEntries)
                if (e.getKey().equals(sortDigit))
                    sb.append(e.getValue());
        return sb.toString();
    }

    private String exampleEncryptB() {

        List<Map.Entry<Integer, Character>> intCharEntries = new ArrayList<>();
        for (int i = 0; i < messageChars.length; i++) {
            int msgCharOrdinal = keyCharOrdinal[i % keyCharOrdinal.length];
            intCharEntries.add(new AbstractMap.SimpleEntry<>(msgCharOrdinal, messageChars[i]));
        }

        return getEntriesTxtByOrder(intCharEntries);
    }

    private String exampleEncryptC() {

        List<Map.Entry<Integer, Character>> intCharEntries = new ArrayList<>();
        int actualChar = 0;
        for (int sortDigit : keyCharOrdinalSort) {

            int keyCharOrdinalPosition = -1;
            for (int i = 0; i < keyCharOrdinal.length; i++)
                if (keyCharOrdinal[i] == sortDigit)
                    keyCharOrdinalPosition = i;

            for (int i = 0; i <= keyCharOrdinalPosition && actualChar < messageChars.length; i++) {
                intCharEntries.add(new AbstractMap.SimpleEntry<>(keyCharOrdinal[i], messageChars[actualChar]));
                ++actualChar;
            }
        }

        return getEntriesTxtByOrder(intCharEntries);
    }

    private String exampleDecryptA() {

        int orderDigitCount = orderDigits.length;
        int[] orderDigitsSort = orderDigits.clone();
        Arrays.sort(orderDigitsSort);
        List<Character> listMessageChar = new ArrayList<>();
        for (char c : messageChars)
            listMessageChar.add(c);

        StringBuilder sb = new StringBuilder();
        Iterator<Character> ci = listMessageChar.iterator();
        while (ci.hasNext()) {
            List<Character> characterPartial = new ArrayList<>();
            int i = 0;
            while (ci.hasNext() && i < orderDigitCount) {
                characterPartial.add(ci.next());
                i++;
            }

            int outOfSize = orderDigitCount - characterPartial.size();
            int ourOfStartIndex = orderDigitCount - outOfSize;
            List<Integer> outOf = new ArrayList<>();

            for (int m = ourOfStartIndex; m < orderDigitCount; m++)
                outOf.add(orderDigitsSort[m]);

            for (int k = 0; k < orderDigits.length; k++) {
                int actualDigit = orderDigits[k];
                if (outOf.contains(actualDigit))
                    characterPartial.add(k, ' ');
            }

            for (int j = 0; j < orderDigitsSort.length; j++) {
                Character c = null;
                for (int k = 0; k < orderDigits.length; k++)
                    if (orderDigitsSort[j] == orderDigits[k]) {
                        c = characterPartial.get(k);
                        break;
                    }
                sb.append(c);
            }

        }

        return sb.toString().trim();
    }

    private String exampleDecryptB() {

        int countChars = messageChars.length;
        int countExtraChars = countChars % keyCharOrdinal.length;
        int standardRowCount = (countChars - countExtraChars) / keyCharOrdinal.length;
        char[][] charMatrix = new char[keyCharOrdinal.length][];

        List<Character> messageCharsList = new ArrayList<>();
        for (Character c : messageChars)
            messageCharsList.add(c);

        Iterator<Character> messageCharsIterator = messageCharsList.iterator();
        for (int sortKeyDigit : keyCharOrdinalSort) {
            for (int i = 0; i < keyCharOrdinal.length; i++) {
                if (sortKeyDigit == keyCharOrdinal[i]) {
                    int rows = standardRowCount;
                    if ((i + 1) <= countExtraChars) rows++;

                    char[] colChars = new char[rows];
                    for (int j = 0; j < colChars.length; j++)
                        colChars[j] = messageCharsIterator.next();
                    charMatrix[i] = colChars;
                }
            }
        }

        StringBuilder sb = new StringBuilder();
        int row = 1;
        OUTER:
        while (true) {
            for (int i = 0; i < charMatrix.length; i++) {
                if (charMatrix[i].length >= row) {
                    sb.append(charMatrix[i][row - 1]);
                } else {
                    break OUTER;
                }
            }
            row++;
        }

        return sb.toString();
    }

    private String exampleDecryptC() {

        int charsCount = messageChars.length;
        List<char[]> tempMatrix = new ArrayList<>();
        OUT:
        for (int i = 0; i < keyCharOrdinalSort.length; i++) {
            for (int j = 0; j < keyCharOrdinal.length; j++) {
                if (keyCharOrdinalSort[i] == keyCharOrdinal[j]) {
                    int ordinalCount = j + 1;
                    if (charsCount > ordinalCount) {
                        charsCount -= ordinalCount;
                        tempMatrix.add(new char[ordinalCount]);
                    } else {
                        tempMatrix.add(new char[charsCount]);
                        break OUT;
                    }
                }
            }
        }

        List<Character> characters = new ArrayList<>();
        for (char c : messageChars)
            characters.add(c);
        Iterator<Character> characterIterator = characters.iterator();

        OUT:
        for (int i = 0; i < keyCharOrdinalSort.length; i++) {

            int columnIndex = -1;

            for (int j = 0; j < keyCharOrdinal.length; j++)
                if (keyCharOrdinalSort[i] == keyCharOrdinal[j])
                    columnIndex = j;

            for (int m = 0; m < tempMatrix.size(); m++) {
                for (int n = 0; n < tempMatrix.get(m).length; n++) {

                    if (columnIndex == n) {
                        char c = characterIterator.next();
                        tempMatrix.get(m)[n] = c;
                    }

                    if (!characterIterator.hasNext())
                        break OUT;
                }
            }

        }

        StringBuilder sb = new StringBuilder();
        for (int m = 0; m < tempMatrix.size(); m++) {
            for (int n = 0; n < tempMatrix.get(m).length; n++) {
                sb.append(tempMatrix.get(m)[n]);
            }
        }

        return sb.toString();
    }

}
