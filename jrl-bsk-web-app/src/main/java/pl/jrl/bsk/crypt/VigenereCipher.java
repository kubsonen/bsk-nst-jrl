package pl.jrl.bsk.crypt;

public class VigenereCipher implements Cryptography {

    private final char[] keyChars;
    private final char[] messageChars;
    private final char[][] alphabetMatrix;
    private final int[] alphabetIndexes;

    public VigenereCipher(String key, String message) {

        if (key == null || message == null)
            throw new RuntimeException("Nor found key or input");

        this.keyChars = key.toUpperCase().toCharArray();
        this.messageChars = message.toUpperCase().replace(" ", "").toCharArray();

        if (key.length() != message.length())
            throw new RuntimeException("Different length of input and key");

        this.alphabetMatrix = new char[CryptUtil.ALPHABET.length][CryptUtil.ALPHABET.length];
        this.alphabetIndexes = new int[CryptUtil.ALPHABET.length];

        for (int i = 0; i < CryptUtil.ALPHABET.length; i++)
            this.alphabetIndexes[i] = CryptUtil.getAlphabetIndex(CryptUtil.ALPHABET[i]);

        for (int i = 0; i < alphabetMatrix.length; i++)
            this.alphabetMatrix[i] = getShiftedAlphabet(i);

    }

    private char[] getShiftedAlphabet(int shift) {
        char[] alphabetShifted = new char[CryptUtil.ALPHABET.length];
        int[] indexesShifted = new int[CryptUtil.ALPHABET.length];
        for (int i = 0; i < alphabetIndexes.length; i++)
            indexesShifted[i] = (alphabetIndexes[i] + shift) % CryptUtil.ALPHABET.length;
        for (int i = 0; i < alphabetIndexes.length; i++)
            alphabetShifted[i] = CryptUtil.ALPHABET[indexesShifted[i]];
        return alphabetShifted;
    }

    public String encrypt() {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < messageChars.length; i++) {
            int messageCharIndex = CryptUtil.getAlphabetIndex(messageChars[i]);
            int keyCharIndex = CryptUtil.getAlphabetIndex(keyChars[i]);
            sb.append(alphabetMatrix[keyCharIndex][messageCharIndex]);
        }

        return sb.toString();
    }

    public String decrypt() {

        StringBuilder sb = new StringBuilder();
        OUTER:
        for (int i = 0; i < keyChars.length; i++) {
            int keyCharIndex = CryptUtil.getAlphabetIndex(keyChars[i]);
            int alphabetIndex = -1;

            for (int j = 0; j < alphabetMatrix[keyCharIndex].length; j++)
                if (messageChars[i] == alphabetMatrix[keyCharIndex][j])
                    alphabetIndex = j;

            sb.append(CryptUtil.ALPHABET[alphabetIndex]);

        }

        return sb.toString();
    }

}
