package pl.jrl.bsk.crypt;

public interface Cryptography {
    String encrypt();
    String decrypt();
}
