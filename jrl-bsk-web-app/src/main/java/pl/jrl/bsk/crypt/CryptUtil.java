package pl.jrl.bsk.crypt;

public class CryptUtil {
    public static final char[] ALPHABET = "abcdefghijklmnopqrstuvwxyz".toUpperCase().toCharArray();

    public static int getAlphabetIndex(char c) {
        for (int i = 0; i < ALPHABET.length; i++) {
            if (ALPHABET[i] == c)
                return i;
        }
        return -1;
    }

}
