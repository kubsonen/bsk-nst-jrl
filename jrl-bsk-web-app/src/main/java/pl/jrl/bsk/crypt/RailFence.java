package pl.jrl.bsk.crypt;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RailFence implements Cryptography {

    private final Long shift;
    private final String message;
    private final int digitTabSize;
    private final int[] digitTab;
    private final char[] messageChars;

    public RailFence(Long shift, String message) {
        this.shift = shift;
        this.message = message;

        if (this.shift < 2)
            throw new RuntimeException("Shift is not higher than 1");

        //Prepare
        digitTabSize = Math.toIntExact(shift + (shift - 2));
        digitTab = new int[digitTabSize];
        for (int i = 1; i <= digitTab.length; i++) {
            if (i <= shift) digitTab[i - 1] = i;
            else if (i > shift) digitTab[i - 1] = Math.toIntExact((2 * shift)) - i;
        }
        messageChars = message.toCharArray();
    }

    public String encrypt() {
        List<Map.Entry<Integer, Character>> intCharEntries = new ArrayList<>();
        for (int i = 0; i < messageChars.length; i++) {
            int digit = digitTab[i % digitTabSize];
            Map.Entry<Integer, Character> entry = new AbstractMap.SimpleEntry<>(digit, messageChars[i]);
            intCharEntries.add(entry);
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 1; i <= shift; i++)
            for (Map.Entry e : intCharEntries)
                if (e.getKey().equals(i))
                    sb.append(e.getValue());

        return sb.toString();
    }

    public String decrypt() {
        int[] decryptDigit = new int[messageChars.length];
        for (int i = 0; i < decryptDigit.length; i++)
            decryptDigit[i] = digitTab[i % digitTabSize];

        char[] decryptChars = new char[messageChars.length];
        for (char encryptChar : messageChars) {

            int lowPos = getTheLowestPositive(decryptDigit);
            int index = -1;
            for (int i = 0; i < decryptDigit.length; i++)
                if (decryptDigit[i] == lowPos) {
                    index = i;
                    break;
                }

            decryptDigit[index] = -1;
            decryptChars[index] = encryptChar;

        }

        StringBuilder sb = new StringBuilder();
        for (char c: decryptChars)
            sb.append(c);

        return sb.toString();
    }

    private int getTheLowestPositive(int[] numbers) {
        int l = -1;
        for (int n : numbers)
            if (n > 0) {
                l = n;
                break;
            }

        if (l == -1)
            throw new RuntimeException("Not found at least one positive number");
        for (int n : numbers)
            if (l > n && n > 0)
                l = n;

        return l;
    }

}
