package pl.jrl.bsk.crypt;

public class CaesarCipher implements Cryptography {

    private final Long shift;
    private final char[] messageChars;
    private final int[] messageCharsIndexes;

    public CaesarCipher(Long shift, String message) {
        this.shift = shift;
        this.messageChars = message.toUpperCase().replace(" ", "").toCharArray();
        this.messageCharsIndexes = new int[messageChars.length];
        for (int i = 0; i < messageChars.length; i++)
            messageCharsIndexes[i] = CryptUtil.getAlphabetIndex(messageChars[i]);
    }

    public String encrypt() {
        int[] messageCharsIndexesShifted = new int[messageCharsIndexes.length];
        for (int i = 0; i < messageCharsIndexes.length; i++)
            messageCharsIndexesShifted[i] = (messageCharsIndexes[i] + Math.toIntExact(shift)) % CryptUtil.ALPHABET.length;

        StringBuilder sb = new StringBuilder();
        for (int shifted : messageCharsIndexesShifted)
            sb.append(CryptUtil.ALPHABET[shifted]);

        return sb.toString();
    }

    public String decrypt() {
        int[] messageCharsIndexesShifted = new int[messageCharsIndexes.length];
        for (int i = 0; i < messageCharsIndexes.length; i++)
            messageCharsIndexesShifted[i] = (messageCharsIndexes[i] + (CryptUtil.ALPHABET.length - Math.toIntExact(shift))) % CryptUtil.ALPHABET.length;

        StringBuilder sb = new StringBuilder();
        for (int shifted : messageCharsIndexesShifted)
            sb.append(CryptUtil.ALPHABET[shifted]);

        return sb.toString();
    }

}
