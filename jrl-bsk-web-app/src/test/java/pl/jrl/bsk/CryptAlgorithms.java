package pl.jrl.bsk;

import org.junit.jupiter.api.Test;
import pl.jrl.bsk.crypt.*;

import java.util.Arrays;

public class CryptAlgorithms {

    @Test
    public void testRailFence() {
        RailFence railFence = new RailFence(3L, "CRYPTOGRAPHY");
        System.out.println(railFence.encrypt());
        railFence = new RailFence(3L, "CTARPORPYYGH");
        System.out.println(railFence.decrypt());
    }

    @Test
    public void testMatrixTransformation() {
        MatrixTransformation mt = new MatrixTransformation("C", "CONVENIENCE", "HERE IS A SECRET MESSAGE ENCIPHERED BY TRANSPOSITION");
        System.out.println(mt.encrypt());

        MatrixTransformation mtDec2 = new MatrixTransformation("A", "3-1-4-2", "YCPRGTROHAYPAOS");
        System.out.println(mtDec2.decrypt());

        MatrixTransformation mtDec3 = new MatrixTransformation("B", "CONVENIENCE", "HECRNCEYIISEPSGDIRNTOAAESRMPNSSROEEBTETIAEEHS");
        System.out.println(mtDec3.decrypt());

        MatrixTransformation mtDec4 = new MatrixTransformation("C", "CONVENIENCE", "HEESPNIRRSSEESEIYASCBTEMGEPNANDICTRTAHSOIEERO");
        System.out.println(mtDec4.decrypt());

    }

    @Test
    public void testCaesarCipher() {
        CaesarCipher caesarCipher = new CaesarCipher(3L, " ACRYPTOGRAPHYZ");
        System.out.println(caesarCipher.encrypt());
        CaesarCipher caesarCipherDec = new CaesarCipher(3L, " DFUBSWRJUDSKBC");
        System.out.println(caesarCipherDec.decrypt());
    }

    @Test
    public void testVigenereCipher() {
        VigenereCipher vigenereCipher = new VigenereCipher("BREAKBREAKBR", "CRYPTOGRAPHY");
        System.out.println(vigenereCipher.encrypt());
        VigenereCipher vigenereCipherDecrypt = new VigenereCipher("BREAKBREAKBR", "DICPDPXVAZIP");
        System.out.println(vigenereCipherDecrypt.decrypt());
    }

    @Test
    public void testConvertMessageToBitArrayTest() {
        String msg = "Jakub Nartowicz";
        for (char c : StreamCipher.convertMessageToBitArray(msg, 0, true)) {
            System.out.print(c);
        }
        System.out.print("\n");
        String g = StreamCipher.convertBitArrayToString("1001010 1100001 1101011 1110101 1100010 100000 1001110 1100001 1110010 1110100 1101111 1110111 1101001 1100011 1111010", 0, false);
        System.out.println("Convert from bit array: " + g);
    }

    @Test
    public void testStreamCipher() {
        StreamCipher streamCipher = new StreamCipher(Arrays.asList(1,2,4), "0110", "Jakub Nartowicz Jan");
        String enc = streamCipher.encrypt();
        StreamCipher streamCipherDec = new StreamCipher(Arrays.asList(1,2,4), "0110", "1111110 1010101 1011111 1000001 1010110 111010 1010100 1111011 1101000 1101110 1110101 1101101 1110011 1111001 1100000 101101 1000111 1101100 1100011");
        String dec = streamCipherDec.decrypt();
        System.out.println(enc);
        System.out.println(dec);
    }

}
